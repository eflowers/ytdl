#!/usr/bin/env python3
import os

from tkinter import *
from tkinter import ttk

from .config import Config

class GUI:

    def main():
        root = Tk()
        root.title("ytdl")

        # Functions are here because GUI.main() needs them locally to run commands and the functions need GUI.main() locally to destroy the window. Therefore, we can't have them in seperate classes.
        def download():
            root.destroy()
            os.system('youtube-dl -o {0}/%(title)s.%(ext)s -f {1}+bestaudio {2} "{3}"'.format(Config.download_folder, ytdl_format.get(), options.get(), yt_url))
        
        def list_formats():
            os.system('youtube-dl {0} -F "{1}"'.format(options.get(), yt_url))
        
        def stream():
            root.destroy()
            os.system('mpv --ytdl-format {0}+bestaudio {1} "{2}"'.format(ytdl_format.get(), options.get(), yt_url))

        def options_stream():
            root.destroy()
            os.system('mpv {0} "{1}"'.format(options.get(), yt_url))

        def options_download():
            root.destroy()
            os.system('youtube-dl {0} "{1}"'.format(options.get(), yt_url))

        yt_url = root.clipboard_get()

        frame = ttk.Frame(root)
        frame.grid()

        # Creates three tabs, one to house webm formats, one to house mp4 formats, and one for command line options, mp3 stream, and possibly more in the future
        filetypes = ttk.Notebook(frame)
        webm = ttk.Frame(filetypes)
        mp4 = ttk.Frame(filetypes)
        misc = ttk.Frame(filetypes)
        filetypes.add(webm, text="webm")
        filetypes.add(mp4, text="mp4")
        filetypes.add(misc, text="misc")
        filetypes.grid()

        ytdl_format = StringVar()

        # webm formats below
        f303 = ttk.Radiobutton(webm, text="1080p, 60fps", variable=ytdl_format, value="303")
        f248 = ttk.Radiobutton(webm, text="1080p", variable=ytdl_format, value="248")
        f302 = ttk.Radiobutton(webm, text="720p, 60fps", variable=ytdl_format, value="302")
        f247 = ttk.Radiobutton(webm, text="720p", variable=ytdl_format, value="247")
        f244 = ttk.Radiobutton(webm, text="480p", variable=ytdl_format, value="244")

        # One apply and test is created per tab for aesthetic reasons, as well as to properly grid apply and test buttons
        stream_button_webm = ttk.Button(webm, text="Stream", command=stream)
        download_button_webm = ttk.Button(webm, text="Download", command=download)
        test_format_webm = ttk.Button(webm, text="List Formats", command=list_formats)

        # mp4 formats below
        f299 = ttk.Radiobutton(mp4, text="1080p, 60fps", variable=ytdl_format, value="299")
        f137 = ttk.Radiobutton(mp4, text="1080p", variable=ytdl_format, value="137")
        f298 = ttk.Radiobutton(mp4, text="720p, 60fps", variable=ytdl_format, value="298")
        f136 = ttk.Radiobutton(mp4, text="720p", variable=ytdl_format, value="136")
        f135 = ttk.Radiobutton(mp4, text="480p", variable=ytdl_format, value="135")

        stream_button_mp4 = ttk.Button(mp4, text="Stream", command=stream)
        download_button_mp4 = ttk.Button(mp4, text="Download", command=download)
        test_format_mp4 = ttk.Button(mp4, text="List Formats", command=list_formats)

        # misc panel
        options = StringVar()
        options_entry = ttk.Entry(misc, textvariable=options)

        options_stream_button = ttk.Button(misc, text="Stream", command=options_stream)
        options_download_button = ttk.Button(misc, text="Download", command=options_download)

        # webm grid
        f303.grid(column=1, row=1, sticky=W)
        f248.grid(column=1, row=2, sticky=W)
        f302.grid(column=1, row=3, sticky=W)
        f247.grid(column=1, row=4, sticky=W)
        f244.grid(column=1, row=5, sticky=W)
        test_format_webm.grid(column=1, row=6)
        stream_button_webm.grid(column=2, row=6)
        download_button_webm.grid(column=3, row=6)

        # mp4 grid
        f299.grid(column=1, row=1, sticky=W)
        f137.grid(column=1, row=2, sticky=W)
        f298.grid(column=1, row=3, sticky=W)
        f136.grid(column=1, row=4, sticky=W)
        f135.grid(column=1, row=5, sticky=W)
        test_format_mp4.grid(column=1, row=6)
        stream_button_mp4.grid(column=2, row=6)
        download_button_mp4.grid(column=3, row=6)

        # misc grid
        options_entry.grid(column=1, row=1, columnspan=3)
        options_stream_button.grid(column=2, row=2)
        options_download_button.grid(column=3, row=2)

        root.mainloop()
